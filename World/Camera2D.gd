extends Camera2D

# For debug, to be able to see all players easier


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var avg_pos = Vector2()
	
	for player in globals.node_Players.get_children():
		avg_pos += player.global_position
	
	position = avg_pos/globals.node_Players.get_child_count()
	print(avg_pos/globals.node_Players.get_child_count())
