extends "res://addons/godot-behavior-tree-plugin/action.gd"


# Leaf Node
func tick(tick):
	var displacement_from_center = tick.actor.position - globals.BOUNDS.size / 2
	var dir_from_center = displacement_from_center.normalized()
	
	var diamond_radius = globals.BOUNDS.size.x * .35
	var move_dir = Vector2()
	# Move in diamond, via quadrants 
	if dir_from_center.y < 0:
		if dir_from_center.x > 0:
			move_dir = (Vector2.UP * diamond_radius - tick.actor.position + globals.BOUNDS.size / 2).normalized() 
		else:
			move_dir = (Vector2.LEFT * diamond_radius - tick.actor.position + globals.BOUNDS.size / 2).normalized() 
	else:
		if dir_from_center.x < 0:
			move_dir = (Vector2.DOWN * diamond_radius - tick.actor.position + globals.BOUNDS.size / 2).normalized() 
		else:
			move_dir = (Vector2.RIGHT * diamond_radius - tick.actor.position + globals.BOUNDS.size / 2).normalized() 
	tick.actor.bot_velocity = move_dir
	
	tick.actor.get_node("Status").text = "Circling"
	return FAILED