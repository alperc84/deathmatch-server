extends "res://addons/godot-behavior-tree-plugin/action.gd"


# Leaf Node
func tick(tick):
	# TODO: Actaully get nearest player
	var nearest_player = tick.actor.visible_enemies.back() 
	tick.actor.bot_target = nearest_player.global_position
	
	tick.actor.is_firing = true
	tick.actor.get_node("Status").text = "Firing"
	
	return FAILED # Exit, don't want to switch to patrolling