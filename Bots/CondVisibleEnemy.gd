extends "res://addons/godot-behavior-tree-plugin/condition.gd"


# Leaf Node
func tick(tick):
	if tick.actor.visible_enemies.size() == 0:
		return OK # No enemies, continue to Patrol
	return FAILED # Continue on to approach