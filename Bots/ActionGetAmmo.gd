extends "res://addons/godot-behavior-tree-plugin/action.gd"


# Leaf Node
func tick(tick):
	if tick.actor.bot.gun.bullet_count == 0 and tick.actor.pickups_ammo.size() > 0:
		var target_pickup = null
		
		# TODO: get closest
		target_pickup = tick.actor.pickups_ammo.back()
		
		tick.actor.bot_velocity = (target_pickup.position - tick.actor.position).normalized()
		tick.actor.get_node("Status").text = "Getting ammo"
		return ERR_BUSY # Exit this tick
	else:
		return FAILED