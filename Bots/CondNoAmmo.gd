extends "res://addons/godot-behavior-tree-plugin/condition.gd"


# Leaf Node
func tick(tick):
	if tick.actor.bot.gun.bullet_count > 0:
		return OK
	return FAILED # Continue on to approach