extends Pickup

var health_ammount = 25


func _ready():
	crate_id = 1


func _on_HealthCrate_body_entered(body):
	if body.has_method("health_pickup"):
		body.health_pickup(health_ammount)
		rpc("pickup")