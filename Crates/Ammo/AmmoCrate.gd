extends Pickup

var ammo_ammount = 20


func _ready():
	crate_id = 0


func _on_AmmoCrate_body_entered(body):
	if body.has_method("ammo_pickup"):
		body.ammo_pickup(ammo_ammount)
		rpc("pickup")