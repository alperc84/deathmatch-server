extends Gun
var gun_id = 1 # Yes it's hack. Copying from database file

remote func fire():
	var caller_id = get_tree().get_rpc_sender_id()
	
	if caller_id == id_player and can_fire and bullet_count > 0:
			
		for bullet_pos in $BulletSpread.get_children():
			var randomish_rot_offset = bullet_pos.position.angle_to(Vector2.UP)
			
			globals.node_world.spawn_bullet(global_position + (bullet_pos.position).rotated(global_rotation), 
					global_rotation - randomish_rot_offset, id_player, ammo_id)
			
		bullet_count -= 1
				
		rearm()
		rpc_id(id_player, "ammo_change", bullet_count)
		rpc_id(id_player, "rearm")

